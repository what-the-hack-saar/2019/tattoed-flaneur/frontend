import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import "./App.css";
import Overview from "./components/Views/Overview";
class App extends Component {
  render() {
    return (
      <div className="App">
        <Grid container>
          <Grid item xs={12}>
            <Overview />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default App;
