import React from "react";
import PropTypes from "prop-types";
import Icon from "@material-ui/core/Icon";

class Location extends React.Component {
  render() {
    const { url, name, address, geo } = this.props;
    return (
      <div itemProp="location" itemScope itemType="http://schema.org/Place">
        {!!url ? (
          <a itemProp="url" href={url}>
            {name}
          </a>
        ) : (
          <span>name</span>
        )}
        {!!geo && (
          <a
            href={`https://www.google.com/maps/search/?api=1&query=${
              geo.latitude
            },${geo.longitude}`}
          >
            <Icon>location_on</Icon>
          </a>
        )}
        {!!address && (
          <div
            itemProp="address"
            itemScope
            itemType="http://schema.org/PostalAddress"
          >
            <span itemProp="streetAddress">{address.streetAddress}</span>
            <br />
            <span itemProp="addressLocality">{address.addressLocality}</span>
          </div>
        )}
      </div>
    );
  }
}

Location.propTypes = {
  geo: PropTypes.object,
  name: PropTypes.string,
  address: PropTypes.string,
  url: PropTypes.string
};

export default Location;
