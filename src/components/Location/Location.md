### Schema

Schema can be found on [Schema.org](https://schema.org/Place).

### Examples

```js
const location = {
  geo: {
    "@type": "GeoCoordinates",
    longitude: 7.00178,
    latitude: 49.23117
  },
  name: "Studio 30",
  address: {
    streetAddress: "Mainzer Stra\u00dfe 30",
    "@type": "PostalAddress",
    postalCode: "66111",
    addressLocality: "Saarbr\u00fccken, Germany"
  },
  url: "https://www.facebook.com/1997666170449951",
  "@type": "Place"
};
<Location {...location} />;
```
