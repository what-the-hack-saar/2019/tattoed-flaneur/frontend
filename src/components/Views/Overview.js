import React from "react";
import Event from "../Event";

/*
 TODO: Query API for items instead of mock data.
 */
class Overview extends React.Component {
  getSchemaComponent(schema) {
    switch (schema["@type"]) {
      /*       case "Event":
        return Event; */
      default:
        return Event;
    }
  }
  render() {
    const items = require("../../__mock__/events.json");
    return (
      <section>
        {items.map((item, index) => {
          const Item = this.getSchemaComponent(item);
          return <Item {...item} key={index} />;
        })}
      </section>
    );
  }
}

Overview.propTypes = {};

export default Overview;
