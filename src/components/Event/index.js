import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Location from "../Location";

const styles = {
  card: {
    minWidth: 275,
    marginBottom: "2rem"
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
};

class Event extends React.Component {
  render() {
    const { startdate, name, description, classes, location } = this.props;
    return (
      <Card
        itemScope
        itemType="http://schema.org/Event"
        className={classes.card}
      >
        <CardHeader
          title={
            <Typography variant="h5" component="h2" itemProp="name">
              {name}
            </Typography>
          }
          subheader={
            <Typography
              className={classes.title}
              color="textSecondary"
              gutterBottom
              itemProp="startDate"
              content={startdate}
            >
              {new Date(startdate).toLocaleDateString("en-US")}{" "}
              {new Date(startdate).toLocaleTimeString("en-US")}
            </Typography>
          }
        />
        <CardContent>
          <Typography component="p" itemProp="description">
            {description}
          </Typography>
          {!!location && <Location {...location} />}
          {/* TODO: Insert offers component*/}
        </CardContent>
      </Card>
    );
  }
}

Event.propTypes = {
  description: PropTypes.string,
  enddate: PropTypes.string,
  location: PropTypes.object,
  name: PropTypes.string.isRequired,
  startdate: PropTypes.string
};

export default withStyles(styles)(Event);
