### Schema

Schema can be found on [Schema.org](https://schema.org/Event).

### Examples

```js
<Event
  startdate="2019-01-11T13:00:00+0100"
  enddate="2019-01-12T23:59:00+0100"
  name="Unifilm Express f\u00fcr Hexerei und Zauberei"
  description="Es ist soweit, ihr habt alle darauf gewartet! Lasst euch von uns mitnehmen, auf eine Reise in die Welt der Hexen, Zauberern, coolen Roben, fliegenden Besen und Socken. \n\nWir teilen den Marathon in 2 Teile auf:\n\nFreitag, 11. Januar\nBeginn: 13 Uhr, Start: 14 Uhr.\nUmfang: Eins-Drei\nEnde: ca. 23:30\n\nSamstag, 12. Januar\nBeginn 10 Uhr, Start 10:30\nUmfang: Vier-Acht\nEnde ca. 23:30\n\nGerade der zweite Teil wird ganz sch\u00f6n hart mit wenig Pausen, also wappnet euch! \n\nDer Eintritt ist frei, ihr k\u00f6nnt auch jederzeit rein und raus.\n\nWir werden kein Essen organisieren sondern nur f\u00fcr Snacks und Getr\u00e4nke sorgen, der Rest ist dann euch selbst \u00fcberlassen.\n\nUm gemeinsam ne sch\u00f6ne gem\u00fctliche Zeit zu haben w\u00e4re es nat\u00fcrlich angebracht etwaige passende Faschings\u00fcberbleibsel mitzubringen.\n\nWir freuen uns auf euch und m\u00f6ge die Macht mit euch sein. Oder so."
/>
```
